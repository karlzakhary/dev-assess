<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    /**
     * @Route("/", name="main")
     */
    public function index()
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
    /**
     * @Route("/api/public", name="public")
     * @return JsonResponse
     */
    public function publicAction(Request $request)
    {

        if ($request->isMethod('POST')) {
            $data = json_decode($request->getContent(), true);
            if (!in_array($data['mode'], array('test', 'production')))
            {
                return new JsonResponse("Error", 400);
            }
            else 
                return new JsonResponse("Success", 200);
        }

        return new JsonResponse("Bad request!", 400);

    }

}
