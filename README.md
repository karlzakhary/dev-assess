# Developer assessment task

The web application is a single page app built with React at the frontend and a Symfony3.4 powered backend API. 


For the backend, you will simply use Symfony to accept and process HTTP requests sent in by React and return the appropriate information based on the value of the sent data.


Traditionally, Symfony usually handles everything from the state management, page rendering and routing when developing web applications with it. Here, a different approach to page rendering, routing and state management was taken by using React. 

Assets built around Webpack are processed and compiled through [Encore's](https://github.com/symfony/webpack-encore) powerful API.


There is only one controller `MainController.php` that implement the endpoint `/api/public` on which react posts data using the following structure 
`{mode: "data"}`, this 'mode' key is the value of the selected payment option.


Our react app has four components: home, loading, success, and error each of them represent a state of the app during the lifecycle of the request.


# How to run project

`cd /dev-assess` 

`composer install`

`yarn install`

` php bin/console server:run`

`npm run dev-server`

