import React, { Component } from "react";

class Fail extends Component {
  constructor() {
    super();

    this.state = { response: "error", step: 1, mode: "", status: "start" };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  //   UpdateStepCount():
  //   is used to pass the current updated state to the parent component 'Home'
  //   to render the 'Form' component by settings parent's state 'Status' key to
  //   "start".

  updateStepCount() {
    this.props.children[1][0](this.state);
  }

  // Class method that gets called on the click of tryAgain button

  handleSubmit() {
    this.updateStepCount();
  }

  render() {
    return (
      <div class="form-validation">
        <div class="form-title-row">
          <span> RaiseNow Setup</span>
        </div>
        <div class="form-title-row-desc">
          <span>
            {" "}
            The RaiseNow Payment Platform requires the following<br></br>{" "}
            parameters to be setup
          </span>
        </div>
        <hr></hr>
        <p>Some error occured please try again!</p>
        <div class="form-row-button">
          <button
            disabled={this.state.submitDisabled}
            type="submit"
            onClick={this.handleSubmit}
          >
            TryAgain
          </button>
        </div>
      </div>
    );
  }
}

export default Fail;
