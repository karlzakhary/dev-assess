import React, { Component } from "react";
import Spinner from "react-spinner-material";
import axios from "axios";

class Loading extends Component {
  constructor(props) {
    super(props);

    this.state = { response: props.children[0], step: 1, mode: "" };
  }

  // When component mounts 'renders' a POST request is made to our symfony API endpoint /api/public
  // containing the selected mode

  componentDidMount() {
    setTimeout(() => {
      axios
        .post(`http://localhost:8000/api/public`, {
          // The passed key 'mode' represents the selected mode in the form whether 'production' or 'test'
          // and it's passed by the parent component 'Home' hence we invoke the props children

          mode: this.props.children[0][1]["mode"]
        })
        .then(response => {
          // Here current state is updated using the parent's props that include the chosen mode
          // and the current step and send the updated state to parent using updateStepCount()

          this.setState({
            response: response.data,
            step: this.props.children[0][1]["step"] + 1,
            status: "success",
            mode: this.props.children[0][1]["mode"]
          });

          this.updateStepCount();
        })

        // In case the server returned error we set the current state's status to error to render 'Error' component
        .catch(error => {
          this.setState({ status: "error" });
          this.updateStepCount();
        });
    }, 1200);
  }

  // ErrorHandle to setState on unmounted component

  componentWillMount() {
    axios.interceptors.request.use(req => {
      return req;
    });
  }

  //   UpdateStepCount():
  //   is used to pass the current updated state to the parent component 'Home'.

  updateStepCount() {
    this.props.children[0][0](this.state);
  }

  render() {
    return (
      <div class="form-validation">
        <div class="form-title-row">
          <span> RaiseNow Setup</span>
        </div>
        <div class="form-title-row-desc">
          <span>
            {" "}
            The RaiseNow Payment Platform requires the following<br></br>{" "}
            parameters to be setup
          </span>
          <Spinner
            id="spinner"
            size={120}
            spinnerColor={"#333"}
            spinnerWidth={0.5}
            visible={true}
          />
        </div>
        <hr></hr>
      </div>
    );
  }
}

export default Loading;
