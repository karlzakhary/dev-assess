import React, { Component } from "react";

class Success extends Component {
  constructor() {
    super();

    this.state = { response: "success", step: 1, mode: "", status: "start" };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  //   UpdateStepCount():
  //   is used to pass the current updated state to the parent component 'Home'
  //   to render the 'Form' component by settings parent's state 'Status' key to
  //   "start".

  updateStepCount() {
    this.props.children[1][0](this.state);
  }

  // Class method that gets called on the click of raiseNow button

  handleSubmit() {
    this.updateStepCount();
  }

  render() {
    return (
      <div className="form-validation">
        <div class="form-title-row">
          <span> RaiseNow Setup</span>
        </div>
        <div class="form-title-row-desc">
          <span>
            {" "}
            The RaiseNow Payment Platform requires the following<br></br>{" "}
            parameters to be setup
          </span>
        </div>
        <hr></hr>
        <p>Successful transaction!</p>
        <div class="form-row-button">
          <button type="submit" onClick={this.handleSubmit}>
            RaiseNow
          </button>
        </div>
      </div>
    );
  }
}

export default Success;
