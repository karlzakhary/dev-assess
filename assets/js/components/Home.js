import React, { Component } from "react";
import { Route, Switch, Redirect, Link, withRouter } from "react-router-dom";
import Form from "./Form";
import Error from "./Error";
import Loading from "./Loading";
import Success from "./Success";

class Home extends Component {
  constructor() {
    super();
    this.state = { status: "start", response: "", step: 1, mode: "" };

    // Binding used function to the lexical context of the class

    this.handleCounter = this.handleCounter.bind(this);
    this.renderSwitch = this.renderSwitch.bind(this);
  }

  // Following function takes care of updating the current component from it's children
  // components when one of them triggers the action by calling the function from its props

  handleCounter(_State) {
    this.setState({
      status: _State.status,
      step: _State.step,
      response: _State.response,
      mode: _State.mode
    });
  }

  // Following function takes care of which component gets mounted based on the param argument
  // that represents the current state's 'Status' key

  renderSwitch(param) {
    const props = [this.handleCounter, this.state];
    switch (param) {

      // Loading component initiates the POST request and renders a Spinner
      // until the server returns response
      case "loading":
        return <Loading>{props} </Loading>;

      // Error component gets mounted in case server returned response with 'error'
      case "error":
        return <Error> {props} </Error>;
      
      // Success component gets mounted in case server returned response with 'success'
      case "success":
        return <Success> {props} </Success>;
      default:
        return;
    }
  }

  render() {
    return (
      <div className="App">
        <header className="navbar navbar-expand-lg navbar-dark">
          <a class="navbar-brand">
            <img
              src={require("../../img/logo-raise-now-inverted.png")}
              srcset={`${require("../../img/logo-raise-now-inverted@2x.png")} 2x, ${require("../../img/logo-raise-now-inverted@3x.png")} 3x`}
              class="Logo_RaiseNow_Inverted"
            ></img>
          </a>
        </header>
        <div id="Rectangle">
          <div id="stepper">
            <span> Step {this.state.step} of 3</span>
            <br></br>
          </div>
        </div>
        <main>
          <div className="row">
            <div className="column side">
              {/* <section> */}
              <a class="brand-img">
                <img src={require("../../img/group-5.png")}></img>
              </a>
              {/* </section> */}
            </div>
            {this.state.status == "start" && (
              <Form>handleCounter={this.handleCounter} </Form>
            )}
            {this.renderSwitch(this.state.status)}
          </div>
        </main>
      </div>
    );
  }
}
export default Home;
