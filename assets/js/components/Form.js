import React, { Component } from "react";

class Form extends Component {
  constructor() {
    super();

    this.state = {
      status: "",
      response: "",
      step: 1,
      submitDisabled: true,
      mode: ""
    };

    // Binding used function to the lexical context of the class

    this.updateStepCount = this.updateStepCount.bind(this);

    this.handleChange = this.handleChange.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // Class method that gets called on the submission of the main form

  handleSubmit() {
    this.updateStepCount();
  }

  //   UpdateStepCount():
  //   is used to pass the current updated state to the parent component 'Home'.

  updateStepCount() {
    
    // The invoked children[1] is the function 'handleCounter(_State)'
    // that is passed as props by the parent component 'Home'
    this.props.children[1](this.state);
  }

  // HandleChange(value):
  // is used to update the component's state when user selects different input
  // among the radio choices 'Test' and 'Production'

  handleChange(value) {
    this.setState({
      mode: value,
      submitDisabled: false,
      status: "loading",
      step: 2
    });
  }

  render() {
    return (
      <form class="form-validation" method="post" onSubmit={this.handleSubmit}>
        <div class="form-title-row">
          <span> RaiseNow Setup</span>
        </div>
        <div class="form-title-row-desc">
          <span> The RaiseNow Payment Platform requires the following</span>
          <br></br>
          <span> parameters to be setup</span>
        </div>
        <hr></hr>

        <div className="form-select-row">
          <label class="rad">
            <input
              type="radio"
              name="react-radio"
              value="test"
              className="form-check-input"
              onClick={event => this.handleChange(event.target.value)}
            />
            <i></i> Test
          </label>
          <br></br>
          <div className="form-check-desc">
            <span> Only transactions in test mode are accepted</span>
          </div>
        </div>
        <br></br>
        <div className="form-select-row">
          <label class="rad">
            <input
              type="radio"
              name="react-raio"
              value="production"
              className="form-check-input"
              onClick={event => this.handleChange(event.target.value)}
            />
            <i></i> Production
          </label>
          <br></br>

          <div className="form-check-desc">
            <span> Only transactions in test mode are accepted</span>
          </div>
        </div>
        <hr></hr>

        <div class="form-row-button">
          <button disabled={this.state.submitDisabled} type="submit">
            Next
          </button>
        </div>
      </form>
    );
  }
}

export default Form;
